#include "RobotControl.h"
#include <iostream>

using namespace std;

RobotControl *rc;

void Menu(int choose) {

	float distance;

	switch (choose) {

	case 1:
		rc->forward(200);
		rc->stopMove();
		cout << "Success" << endl;
		break;
	case 2:
		rc->turnLeft();
		rc->stopTurn();
		cout << "Success" << endl;
		break;
	case 3:
		rc->turnRight();
		rc->stopTurn();
		cout << "Success" << endl;
		break;
	case 4:
		cout << "Please enter a distance : ";
		cin >> distance;
		rc->forward(distance);
		rc->stopMove();
		cout << "Success" << endl;
		break;
	case 5:
		cout << "Please enter a distance : ";
		cin >> distance;
		rc->backward(distance);
		rc->stopMove();
		cout << "Success" << endl;
		break;
	case 6:
		break;
	default:
		cout << "\nWrong choose!!Please try again...\n";
		break;

	}
	
}

int main() {

	
	PioneerRobotAPI *robot = new PioneerRobotAPI;
	RobotControl robot_control(robot);
	rc = &robot_control;

	int choose;
	
	do {

		system("cls");
		cout << "1- Move Robot" << endl;
		cout << "2- Turn Left" << endl;
		cout << "3- Turn Right" << endl;
		cout << "4- Move Forward in certain distance(mm)" << endl;
		cout << "5- Move Backward in certain distance(mm)" << endl;
		cout << "6- Exit" << endl;

		cin >> choose;

		Menu(choose);

		system("pause");

	} while (choose != 6);

	system("pause");
	return 0;

}