#include<iostream>
#include<fstream>
#include "RobotControl.h"

using namespace std;


RobotControl::RobotControl(RobotInterface *robot, RobotOperator *ro) {

	robotAPI = robot;
	robot_operator = ro;
	path = new Path();
}
RobotControl::~RobotControl() {

}

Pose RobotControl::getPose() {

	return robotAPI->getPose();

}

void RobotControl::setPose(Pose *position) {

	robotAPI->setPose(position);


}

void RobotControl::turnLeft() {

	robotAPI->turnLeft();
}

void RobotControl::turnRight() {

	robotAPI->turnRight();
}

void RobotControl::forward(float speed) {

	robotAPI->forward(speed);
}

void RobotControl::backward(float speed) {

	robotAPI->backward(speed);

}

void RobotControl::stopMove() {

	robotAPI->stopMove();

}

void RobotControl::stopTurn() {

	robotAPI->stopTurn();

}

void RobotControl::print() {

	robotAPI->print();

}

bool RobotControl::addToPath() {

	int previous_size = path->getNumber();
	path->addPos(getPose());
	int next_size = path->getNumber();

	if (next_size - previous_size == 1) {
		return true;
	}
	else {
		return false;
	}

}

bool RobotControl::clearPath() {

	for (int i = 0; i < path->getNumber(); i++) {

		path->removePos(i);

	}

	if (path->getNumber() == 0) {
		return true;
	}
	else {
		return false;
	}
}

bool RobotControl::recordPathToFile() {

	Record record;
	record.openFile();
	record.setFileName("record.txt");

	for (int i = 0; i < path->getNumber(); i++) {

		record << "X : " << to_string(path->getPos(i).getX()) << "Y : " << to_string(path->getPos(i).getY()) <<
			"Angle : " << to_string(path->getPos(i).getTh()) << "\n";

	}
	record.closeFile();

	return true;
}

bool RobotControl::openAccess(int password) {

	bool check = robot_operator->checkAccessCode(password);

	if (check) {

		accessStatus = true;
		return accessStatus;
	}
	else {

		accessStatus = false;
		return accessStatus;
	}

}

bool RobotControl::closeAccess(int password) {

	bool check = robot_operator->checkAccessCode(password);

	if (check) {

		accessStatus = true;
		return accessStatus;
	}
	else {

		accessStatus = false;
		return accessStatus;
	}

}

bool RobotControl::getAccessStatus() {

	return accessStatus;

}

