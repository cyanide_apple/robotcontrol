#ifndef ROBOTCONTROL_H
#define ROBOTCONTROL_H

#include "Pose.h"
#include "RobotInterface.h"
#include "RobotOperator.h"
#include "Path.h"

/**
* @file RobotControl.h
* @Author Veysel K Oz 
* @brief to control behaviour of robot such like move,turn etc. 
*
*/

class RobotControl {

public:
	/*!
	*  \fn RobotControl::RobotControl()
	*  \brief A constructor function.
	* */
	RobotControl(RobotInterface*,RobotOperator*);
	/*!
	*  \fn RobotControl::~RobotControl()
	*  \brief A destructor function.
	* */
	~RobotControl();
	/*!
	*   \fn void RobotControl::turnLeft()
	*   \brief to turn left robot.
	*
	* */
	void turnLeft();
	/*!
	*   \fn void RobotControl::turnRight()
	*   \brief to turn right robot.
	*
	* */
	void turnRight();
	/*!
	*   \fn void RobotControl::forward(float speed)
	*   \brief to move forward robot.
	*   \param speed of robot.
	*
	* */
	void forward(float);

	/*!
	*   \fn void RobotControl::print()
	*   \brief to print information about robot.
	*
	* */
	void print();
	/*!
	*   \fn void RobotControl::backward(float speed)
	*   \brief to move backward robot.
	*   \param speed of robot.
	*
	* */
	void backward(float);
	/*!
	*	\fn Pose RobotControl::getPose()
	*   \brief return position of robot.
	* */
	Pose getPose();
	/*!
	*   \fn void RobotControl::setPose(Pose position)
	*   \brief set position of robot.
	*	\param position an object of Pose class.
	* */
	void setPose(Pose*);
	/*!
	*   \fn void RobotControl::stopTurn()
	*   \brief to stop turn robot.
	*
	* */
	void stopTurn();
	/*!
	*   \fn void RobotControl::stopMove()
	*   \brief to stop robot.
	*
	* */
	void stopMove();

	/*!
	*	\fn bool RobotControl::addToPath()
	*	\brief to add pose into path.
	*	\detail if pose is added,function return true.
	*/
	bool addToPath();

	/*!
	*	\fn bool RobotControl::clearPath()
	*	\brief to clean up pose on path.
	*	\detail if pose is cleaned,function return true.
	*/
	bool clearPath();

	/*!
	*	\fn	bool RobotControl::recordPathToFile()
	*	\brief to save pose in file.
	*	\detail if pose is saved,function return true.
	*/
	bool recordPathToFile();

	/*!
	*	\fn bool RobotControl::openAccess(int)
	*	\brief to open access functions of application.
	*	\detail if entered password is true,all functions are enable otherwise not.
	*
	*/
	bool openAccess(int);

	/*!
	*	\fn bool RobotControl::openAccess(int)
	*	\brief to close access functions of application.
	*	\detail if entered password is true,all functions are disable otherwise not.
	*	
	*/
	bool closeAccess(int);

	/*!
	*	\fn bool RobotControl::getAccessStatus()
	*	\brief if return true,user can access operations and functions.
	*		
	*/
	bool getAccessStatus();

private:
	RobotInterface *robotAPI;
	int state;
	RobotOperator *robot_operator;
	Path *path;
	bool accessStatus = false;
};

#endif // !ROBOTCONTROL_H
